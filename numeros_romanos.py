def IntPequenoARom(entero):
    listaR = [(1000, "M"),(900, "CM"),(500, "D"),(400, "CD"),(100, "C"),(90, "XC"),(50, "L"),(40, "XL"),(10, "X"),(9, "IX"),(5, "V"),(4, "IV"),(1, "I")]
    ResultadoRomano = ""
    for NumeroCompleto in listaR:
            while entero >= NumeroCompleto[0]:
                    entero -= NumeroCompleto[0]
                    ResultadoRomano += NumeroCompleto[1]
    return ResultadoRomano

def IntGrandeARom(entero):
    miles, resto = divmod(entero, 1000)
    return "({}){}".format(IntPequenoARom(miles), IntPequenoARom(resto))

def intToRoman(entero):
    if entero >= 4000:
        return IntGrandeARom(entero)
    else:
        return IntPequenoARom(entero)

print("Ingrese un numero: ")
entero = int(raw_input())
print(intToRoman(entero))